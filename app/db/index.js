'use strict';
const config = require('../config');
const Mongoose = require('mongoose').connect(config.dbURI);

Mongoose.connection.on('error',err=>{
    console.log("0000000000000000000000000000000000000000000000000000000");
    console.error(err);
    console.log("0000000000000000000000000000000000000000000000000000000");
});
//Creating Schema
const chatUser = new Mongoose.Schema({
    profileId:String,
    fullName:String,
    profilePic:String
});

//Turn the Schema into a Collection in DB
let userModel = Mongoose.model('chatUser',chatUser);
module.exports = {
    Mongoose,
    userModel
}