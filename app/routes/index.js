'use strict';
const h = require('../helper');
const passport =  require('passport');
const config = require('../config');
module.exports = () => {
    let routes = {
        //all get methods;
        'GET' : {
            '/': (req,res,next)=>{
                    res.render('login',{
                    'pageTittle' : 'My Login Page'
                 });
            },
            '/rooms': [h.isAuthenticated,(req,res,next)=>{
                    res.render('rooms',{
                        user: req.user,
                        host: config.host
                    });
            }],
            '/chat/:id': [h.isAuthenticated,(req,res,next)=>{
                let getRoom = h.findRoomById(req.app.locals.chatRooms,req.params.id);
                console.log(req.params.id);
                console.log(getRoom);
                if(getRoom===undefined){
                    return next();
                }else{
                    res.render('chatroom',{
                        user: req.user,
                        host: config.host,
                        room: getRoom.room,
                        roomID: getRoom.roomID
                    });
                }
            }],
            
            '/auth/fb' : passport.authenticate('facebook'),
            
            '/auth/fb/callback' : passport.authenticate('facebook',{
                "successRedirect":'/rooms',
                'failureRedirect':'/'
            }),
            '/auth/twitter' : passport.authenticate('twitter'),
            
            '/auth/twitter/callback' : passport.authenticate('twitter',{
                "successRedirect":'/rooms',
                'failureRedirect':'/'
            }),
            '/logout': (req,res,next)=>{
                    req.logout();
                    res.redirect('/');
            }
        },
        'POST':{
        },
        'NA': (req,res,next)=>{
                    res.status(404).sendFile(process.cwd() + '/views/404.htm');
        }
    }
    //iteration thru all objects
    return h.route(routes);
}