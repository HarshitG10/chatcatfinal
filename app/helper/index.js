'use strict'
const router = require('express').Router();
const db = require('../db');
const crypto = require('crypto');
let regrRoute = (r,m)=>{
    for(let k in r){
        //console.log(k);
        //console.log(typeof r[k] === 'object' && r[k] !== null && !(r[k] instanceof Array));
        if(typeof r[k] === 'object' & r[k] !== null & !(r[k] instanceof Array))
        {
            //console.log(r[k]," instanceof Array " + (r[k] instanceof Array));
            regrRoute(r[k],k);
        }else
        {
            if(m == 'GET')
            {
                router.get(k,r[k]); 
                //console.log( "method is " + m,'route function is ' + r[k]);
            }else if (m == 'POST'){
                router.post(k,r[k]);
                //console.log( m,r[k]);
            }else
            {
                router.use(r[k ]);
            }
        }
    }
}

let route = routes=>{
    regrRoute(routes);
    return router
}

let findOne = profileID =>{
    return db.userModel.findOne({'profileId':profileID});
}

// Create a new user and return its instance
let createNewUser = profile =>{
    return new Promise((resolve,reject)=>{
        let newChatUser = new db.userModel({
            profileId: profile.id,
            fullName: profile.displayName,
            profilePic: profile.photos[0].value || ''
        });
        newChatUser.save(error=>{
            if(error){
                console.log("error creating new user");
                reject(error);
            }else{
                resolve(newChatUser);
            }
        });
    })
}
// finding user into db
let findById= id=>{
    return new Promise((resolve,reject)=>{
        db.userModel.findById(id,(error,user)=>{
            if(error){
                reject(error);
            }else
            {
                resolve(user);
            }
        });
    });
}
let isAuthenticated = (req,res,next)=> {
    if(req.isAuthenticated()){
        next();
    }else{
        res.redirect('/');
    }
}

let randomHex = () => {
	return crypto.randomBytes(24).toString('hex');
}


let addUserToRoom = (allrooms, data, socket) => {
	let getRoom = findRoomById(allrooms, data.roomID);
	if(getRoom !== undefined) {
		let userID = socket.request.session.passport.user;
		let checkUser = getRoom.users.findIndex((element, index, array) => {
			if(element.userID === userID) {
				return true;
			} else {
				return false;
			}
		});


		if(checkUser > -1) {
			getRoom.users.splice(checkUser, 1);
		}

		getRoom.users.push({
			socketID: socket.id,
			userID,
			user: data.user,
			userPic: data.userPic
		});

		socket.join(data.roomID);
		return getRoom;
	}
}


let removeUserFromRoom = (allrooms, socket) => {
	for(let room of allrooms) {
		let findUser = room.users.findIndex((element, index, array) => {
			if(element.socketID === socket.id) {
				return true;
			} else {
				return false;
			}
		});

		if(findUser > -1) {
			socket.leave(room.roomID);
			room.users.splice(findUser, 1);
			return room;
		}
	}
}


let findRoomById = (allRoom,roomID)=>{
    let findRoom = allRoom.find((element,index,array)=>{
        console.log(element.roomID === roomID);

        if(element.roomID === roomID){
            return true;
        }else{
            return false;
        }
    });
    return findRoom;
}


let findRoomByName = (allRoom,room)=>{
    let findRoom = allRoom.findIndex((element,index,array)=>{
        if(element.room===room){
            return true;
        }else{
            return false;
        }
    });
    return findRoom >-1 ? true : false;
}
module.exports ={
    route : route,
    findOne,
    createNewUser,
    findById,
    isAuthenticated,
    findRoomByName,
    randomHex,
    findRoomById,
    removeUserFromRoom,
    addUserToRoom
}