'use strict';
const passport = require('passport');
const config = require('../config');
const h = require('../helper');
const FacebookStrategy =  require('passport-facebook').Strategy;
const TwitterStrategy =  require('passport-twitter').Strategy;
module.exports = ()=>{
    passport.serializeUser((user,done)=>{
        done(null,user.id);
    });

    passport.deserializeUser((is,done)=>{
        h.findById(is)
            .then(user=>done(null,user))
            .catch(error=>console.error(err));
    });
    let authProcessor = (accessToken,refreshToken,profile,done)=>{
        h.findOne(profile.id)
            .then(result=>{
                if(result){
                    done(null,result);
                }else{
                    //create new Profile
                    h.createNewUser(profile)
                        .then(newChatUser=>done(null));
                }
            });
    }
    passport.use(new FacebookStrategy(config.fb,authProcessor));
    passport.use(new TwitterStrategy(config.twitter,authProcessor));
}