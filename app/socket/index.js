'use strict';
const h = require('../helper');

module.exports = (io,app) =>{
    let allrooms = app.locals.chatRooms;
    io.of('/roomlist').on('connection',socket=>{
        socket.on('getChatRoom',()=>{
            socket.emit('chatRoomLst',JSON.stringify(allrooms));
        });
        socket.on('createNewRoom',name=>{
            if(!h.findRoomByName(allrooms,name)){
                allrooms.push({
                    room : name,
                    roomID : h.randomHex(),   
                    users : []
                });
                
                socket.emit('chatRoomLst',JSON.stringify(allrooms));

                socket.broadcast.emit('chatRoomLst', JSON.stringify(allrooms));
            }

        });
    });
    
    io.of('/chatter').on('connection', socket => {
    socket.on('join', data => {
        console.log( socket);
        let usersList = h.addUserToRoom(allrooms, data, socket);
        console.log(usersList);
        socket.broadcast.to(data.roomID).emit('updateUsersList', JSON.stringify(usersList.users));

        socket.emit('updateUsersList', JSON.stringify(usersList.users));
    });

    socket.on('disconnect', () => {
        let room = h.removeUserFromRoom(allrooms, socket);
        socket.broadcast.to(room.roomID).emit('updateUsersList', JSON.stringify(room.users));
    });

    socket.on('newMessage', data => {
        socket.to(data.roomID).emit('inMessage', JSON.stringify(data));
    });
    });
}